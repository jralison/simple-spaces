<?php

namespace Jralison\SimpleSpaces;

use Aws\S3\S3Client;

class SimpleSpacesClient {

    /** @var string */
    private $accessKey;

    /** @var string */
    private $secretKey;

    /** @var string */
    private $spacesName;

    /** @var string */
    private $region;

    /** @var string */
    private $host;

    /** @var string */
    private $endpoint;

    /** @var S3Client */
    private $client;

    /**
     * SimpleSpaces constructor.
     *
     * @param string $accessKey
     * @param string $secretKey
     * @param string $spacesName
     * @param string $region
     * @param string $host
     */
    public function __construct(string $accessKey, string $secretKey, string $spacesName = '', string $region = 'nyc3', string $host = 'digitaloceanspaces.com') {
        $this->accessKey = $accessKey;
        $this->secretKey = $secretKey;
        $this->spacesName = $spacesName;
        $this->region = $region;
        $this->host = $host;
        $this->endpoint = "https://" . ($spacesName ? "{$spacesName}." : '') . "{$region}.{$host}";

        $this->client = new S3Client([
            'version' => 'latest',
            'bucket_endpoint' => true,
            'credentials' => [
                'key' => $accessKey,
                'secret' => $secretKey,
            ],
            'region' => $region,
            'endpoint' => $this->endpoint,
        ]);
    }

    /**
     * @param string $key
     * @return null|string
     */
    public function getObject(string $key): ?string {
        try {
            $object = $this->client->getObject([
                'Bucket' => $this->spacesName,
                'Key' => $key,
            ]);
            return $object->get('Body')->getContents();
        } catch (\Exception $exception) {
            return null;
        }
    }

    /**
     * @param string $prefix
     * @return string[]
     */
    public function getObjectList(string $prefix = ''): array {
        $object = $this->client->listObjects([
            'Bucket' => $this->spacesName,
            'Prefix' => $prefix,
        ]);
        $keys = array_column($object->get('Contents') ?? [], 'Key');
        return $keys;
    }

    /**
     * @param string $key
     * @param string $contents
     * @return mixed
     */
    public function putObject(string $key, string $contents): bool {
        try {
            $this->client->putObject([
                'Bucket' => $this->spacesName,
                'Key' => $key,
                'Body' => $contents,
            ]);
            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function deleteObject(string $key): bool {
        try {
            $this->client->deleteObject([
                'Bucket' => $this->spacesName,
                'Key' => $key,
            ]);
            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }

}
