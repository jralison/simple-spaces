<?php

namespace spec\Jralison\SimpleSpaces;

use Jralison\SimpleSpaces\SimpleSpacesClient;
use PhpSpec\ObjectBehavior;

class SimpleSpacesClientSpec extends ObjectBehavior {

    private $accessKey;
    private $secretKey;
    private $spacesName;
    private $testObjectContent = '{"test": true}';
    private $testObjectKeyPrefix = 'test/';
    private $testObjectKey = "test/test.json";
    private $testNonExistentObjectKey = "test/unknown.json";

    function let() {
        $this->accessKey = getenv('SPACES_ACCESS_KEY');
        $this->secretKey = getenv('SPACES_SECRET_KEY');
        $this->spacesName = getenv('SPACES_BUCKET_NAME');
        $this->beConstructedWith($this->accessKey, $this->secretKey, $this->spacesName);
    }

    function it_is_initializable() {
        $this->shouldHaveType(SimpleSpacesClient::class);
    }

    function it_puts_objects_onto_spaces() {
        $this
            ->putObject($this->testObjectKey, $this->testObjectContent)
            ->shouldBe(true);
    }

    function it_returns_false_on_putting_invalid_object_onto_spaces() {
        $this
            ->putObject('', '')
            ->shouldBe(false);
    }

    function it_lists_objects() {
        $this
            ->getObjectList($this->testObjectKeyPrefix)
            ->shouldReturn([$this->testObjectKey]);
    }

    function it_gets_an_object_from_spaces() {
        $this
            ->getObject($this->testObjectKey)
            ->shouldBeEqualTo($this->testObjectContent);
    }

    function it_returns_null_on_getting_invalid_object() {
        $this
            ->getObject($this->testNonExistentObjectKey)
            ->shouldBe(null);
    }

    function it_deletes_objects() {
        $this
            ->deleteObject($this->testObjectKey)
            ->shouldReturn(true);
    }

    function it_returns_false_on_delete_invalid_object() {
        $this
            ->deleteObject('')
            ->shouldReturn(false);
    }

    function it_lists_empty_after_delete() {
        $this
            ->getObjectList($this->testObjectKeyPrefix)
            ->shouldReturn([]);
    }

}
